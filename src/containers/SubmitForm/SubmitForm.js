import React,{Component} from 'react';

class SubmitForm extends Component {
    state = { term: '' };
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.state.term === '') return;
        this.props.onFormSubmit(this.state.term);
        this.setState({ term: '' });
        }

    render() {
        return(
            <form onSubmit={this.handleSubmit}>
                <input
                    type='text'
                    className='filmName'
                    placeholder='Введите название фильма'
                    value={this.state.term}
                    onChange={(e) => this.setState({term: e.target.value})}
                />
                <button className='button'>Submit</button>
            </form>
        );
    }
}

export default SubmitForm;