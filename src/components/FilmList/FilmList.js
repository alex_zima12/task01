import React from 'react';
import Film from "../Film/Film.js"

const FilmList = (props) => {
    const films = props.films.map((film, index) => {
        return <Film content={film} key={index} index={index} handleChangeName={props.handleChangeName} onDelete={props.onDelete} />
    })
    return(
        <div className='list-wrapper'>
            {films}
        </div>
    );
}

export default FilmList;