import React, {Component} from 'react';

class Film  extends Component {
      shouldComponentUpdate(nextProps, nextState) {
          return nextProps.content !== this.props.content
     }

    render() {
        return(
            <div className='list-item'>
              <input
                  value={this.props.content}
                  className='filmName'
                   onChange={(e) => {
                       this.props.handleChangeName(this.props.index,e.target.value);
                       }}
              />
                <button class="button" onClick={() => {this.props.onDelete(this.props.index)}}>Delete</button>
            </div>
    )}
}

export default Film;
