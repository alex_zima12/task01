import React from 'react';

const Header = (props) => {
    return(
        <div >
            <h2 className='header'>
                К просмотру {props.numFilms} фильмов
            </h2>
        </div>
    );
}

export default Header;