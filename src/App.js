import React,{Component} from 'react';
import './App.css';
import Header from "./components/Header/Header"
import FilmList from "./components/FilmList/FilmList"
import SubmitForm from "./containers/SubmitForm/SubmitForm"

class App extends Component {
  state = {
    films: ['Гарри Поттер и Кубок Огня', 'Большой куш', 'Убить Билла']
  };

  handleSubmit = film => {
    this.setState({films: [...this.state.films, film]});
  }

  handleDelete = (index) => {
    const newArr = [...this.state.films];
    newArr.splice(index, 1);
    this.setState({films: newArr});
  }

  handleChangeName = (idexElem,name) => {
    this.setState({films: this.state.films.map((el,index)=>index===idexElem ? name:el)});
  }

  render() {
    return(
        <div className='wrapper'>
          <div className='frame'>
            <Header numFilms={this.state.films.length} />
            <SubmitForm onFormSubmit={this.handleSubmit} />
            <FilmList films={this.state.films} handleChangeName={this.handleChangeName} onDelete={this.handleDelete} />
          </div>
        </div>
    );
  }
}



export default App;
